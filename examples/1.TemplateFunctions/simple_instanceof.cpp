#include <iostream>

class Animal
{
public:
	virtual ~Animal() = default;
};

class Dog : public Animal{};

class Cat : public Animal {};

class Boat 
{
public:
	virtual ~Boat() = default;
};

class Yacht : public Boat{};

template <typename Derived, typename Base>
bool instanceof(Derived* const derived)
{
	return dynamic_cast<Base*>(derived) != nullptr;
}

int main()
{
	Cat* kitty = new Cat();
	if (instanceof<Cat, Animal>(kitty))
	{
		std::cout << "Kitty is also an animal!" << std::endl;
	}

	Yacht* boat = new Yacht();
	auto boatCastResult = dynamic_cast<Animal*>(boat);
	if (boatCastResult)
		std::cout << "Boat is also an animal!" << std::endl;

	delete kitty;
	delete boat;

	//How can we turn these type checks in a template function?
	return 0;
}