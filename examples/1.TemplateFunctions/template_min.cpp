#include <iostream>

namespace templates_course
{
	template <typename T>
	T min(const T& first, const T& second)
	{
		return (first < second) ? first : second;
	}
	//Can we do better?
}

int main()
{
	//minimum of 2 integers
	std::cout << templates_course::min(1, 2) << std::endl;
	std::cout << templates_course::min(5, 2) << std::endl;
	std::cout << templates_course::min(123, 123) << std::endl;

	////minimum of 2 floats
	//std::cout << templates_course::min(7.2f, 8.2f) << std::endl;
	//std::cout << templates_course::min(5.2f, 23.4f) << std::endl;
	//std::cout << templates_course::min(123.0f, 123.0f) << std::endl;

	////minimum of 2 doubles
	//std::cout << templates_course::min(7.2, 8.2) << std::endl;
	//std::cout << templates_course::min(5.2, 23.4) << std::endl;
	//std::cout << templates_course::min(123.0, 123.0) << std::endl;

	////minimum of 2 strings
	//std::cout << templates_course::min(std::string("abcd"), std::string("azd")) << std::endl;
	//std::cout << templates_course::min(std::string("CPlusPlus"), std::string("C")) << std::endl;

	return 0;
}