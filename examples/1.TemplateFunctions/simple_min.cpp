#include <iostream>

namespace templates_course
{
	int min(int first, int second)
	{
		return (first < second) ? first : second;
	}

	float min(float first, float second)
	{
		return (first < second) ? first : second;
	}

	double min(double first, double second)
	{
		return (first < second) ? first : second;
	}
}

//int main()
//{
//	//minimum of 2 integers
//	std::cout << templates_course::min(1, 2) << std::endl;
//	std::cout << templates_course::min(5, 2) << std::endl;
//	std::cout << templates_course::min(123, 123) << std::endl;
//
//	//minimum of 2 floats
//	std::cout << templates_course::min(7.2f, 8.2f) << std::endl;
//	std::cout << templates_course::min(5.2f, 23.4f) << std::endl;
//	std::cout << templates_course::min(123.0f, 123.0f) << std::endl;
//
//	//minimum of 2 doubles
//	std::cout << templates_course::min(7.2, 8.2) << std::endl;
//	std::cout << templates_course::min(5.2, 23.4) << std::endl;
//	std::cout << templates_course::min(123.0, 123.0) << std::endl;
//
//	return 0;
//}