#include <iostream>
#include <vector>
#include <numeric>

namespace templates_course
{
	template <typename T>
	T addNumbers(const std::vector<T>& numbers)
	{
		return std::accumulate(std::cbegin(numbers), std::cend(numbers), 0.0);
	}

	template <>
	double addNumbers<double>(const std::vector<double>& numbers)
	{
		return std::accumulate(std::cbegin(numbers), std::cend(numbers), 0.0);
	}


}
//
//int main()
//{
//	std::cout << templates_course::addNumbers(std::vector<int>{1, 2}) << '\n'
//		<< templates_course::addNumbers(std::vector<int>{1, 2, 3}) << '\n'
//		<< templates_course::addNumbers(std::vector<double>{1.2, 2.023, 3.05, 4.45}) << '\n';
//
//	//The last one has a precision problem. Why? How could we solve it?
//
//	return 0;
//}