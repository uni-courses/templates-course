#include <iostream>
#include <type_traits>
#include <fstream>

namespace templates_course
{
	template <typename T>
	T addNumbers(T value) 
	{
		return value;
	}

	template <typename T, typename... Ts>
	T addNumbers(T value, Ts... values)
	{
		return value + addNumbers(values...);
	}

	// What if we constrainted the T type?

	/*template <typename T, typename... Ts>
	std::enable_if_t<std::is_floating_point_v<T> || std::is_integral_v<T>, T> addNumbers(T value, Ts... values)
	{
		return value + addNumbers(values...);
	}*/
	
	template <typename T>
	void writeAuthors(std::ostream& outputStream, T author)
	{
		outputStream << author << '\n';
	}

	template <typename T, typename... Ts>
	void writeAuthors(std::ostream& outputStream, T author, Ts... authors)
	{
		writeAuthors(outputStream, author);
		writeAuthors(outputStream, authors...);
	}
}

//int main()
//{
//	std::cout << templates_course::addNumbers(1, 2, 3, 4, 5, 6) << '\n'
//			  << templates_course::addNumbers(1.2, 2.023, 3.05, 4.45) << '\n';
//
//	//The function also works on strings. 
//
//	std::cout << templates_course::addNumbers(std::string{ "abc" }, std::string{ "def" }) << '\n';
//
//	// What if we wanted to 
//
//	std::ofstream outputFileStream("out.txt");
//	templates_course::writeAuthors(outputFileStream, "Author A", "Author B", "Author C", "Author D");
//
//	std::ofstream outputFileStream2("out2.txt");
//	templates_course::writeAuthors(outputFileStream2, "Stephen King", "Michael Connelly");
//
//	return 0;
//}