#include <iostream>

namespace templates_course
{
	template <typename T>
	class GenericPair
	{
	public:
		GenericPair(const T& first, const T& second) :
			m_first(first),
			m_second(second)
		{}

		const T& getFirst() const noexcept { return m_first; }
		const T& getSecond() const noexcept { return m_second; }

	private:
		T m_first;
		T m_second;
	};
}

//int main()
//{
//	templates_course::GenericPair someIntegersPair(1985, 34);
//	std::cout << "First component: " << someIntegersPair.getFirst()
//			  << std::endl << "Second component: " << someIntegersPair.getSecond()
//			  << std::endl;
//
//	templates_course::GenericPair someStringPair(std::string("Modern"), std::string("C++"));
//	std::cout << "First component: " << someStringPair.getFirst()
//			  << std::endl << "Second component: " << someStringPair.getSecond()
//			  << std::endl;
//	//What if I wanted the first component to be integer, and the second to be string?
//	return 0;
//}