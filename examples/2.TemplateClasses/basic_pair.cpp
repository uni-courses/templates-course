#include <iostream>
#include <vector>

namespace templates_course
{
	template <typename T>
	class IntegersPair
	{
	public:
		IntegersPair(const T& first, const T& second) :
			m_first(first),
			m_second(second)
		{}

		int getFirst() const { return m_first; }
		int getSecond() const { return m_second; }

	private:
		T m_first;
		T m_second;
	};
}

int main()
{
	templates_course::IntegersPair someIntegersPair(1985, 34);
	std::cout << "First component: " << someIntegersPair.getFirst()
			  << std::endl << "Second component: " << someIntegersPair.getSecond() 
			  << std::endl;

	templates_course::IntegersPair someStringPair(std::string{ "Modern" }, std::string{ "C++" });
	templates_course::IntegersPair someStringPair2(std::vector<int>{1, 2, 3}, std::vector<int>{1,2,3});


	//templates_course::IntegersPair someStringPair3(std::string{ "Modern" }, std::vector<int>{1,2,3});
	//Obviously this won't work, what would be some solutions?
	return 0;
}