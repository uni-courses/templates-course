#include <iostream>

#include <sstream>
#include <limits>
#include <iomanip>

namespace templates_course
{
	template <typename T>
	std::string toString(const T& value)
	{
		std::ostringstream outputStringStream;
		outputStringStream << value;
		return outputStringStream.str();
	}

	template <>
	std::string toString<bool>(const bool& value)
	{
		std::ostringstream outputStringStream;
		outputStringStream << std::boolalpha << value;
		return outputStringStream.str();
	}

	class MyClass{
	public:
		int getId() const { return 0; }
	};

	template <>
	std::string toString<MyClass>(const MyClass& value)
	{
		std::ostringstream outputStringStream;
		outputStringStream << typeid(value).name();
		return outputStringStream.str();

		//reflexion != C++
		// RTTI - runtime type information
	}
}

//int main()
//{
//	int someInteger{ 5 };
//	std::string someIntegerString = templates_course::toString(someInteger);
//	std::cout << someIntegerString << std::endl;
//
//	std::cout << templates_course::toString(true) << std::endl;
//
//	double someBigDoubleValue = std::numeric_limits<double>::min();
//	auto someBigDoubleValueString = templates_course::toString(someBigDoubleValue);
//	std::cout << someBigDoubleValueString << std::endl;
//
//	std::cout << templates_course::toString(templates_course::MyClass()) << std::endl;
//	//How to make this compile?
//	return 0;
//}