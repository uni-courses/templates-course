#include <iostream>

#include <string>
#include <unordered_set>

namespace templates_course
{
	class Song
	{
	public:
		Song(const std::string& name, int durationInMinutes):
			m_name(name),
			m_durationInMinutes(durationInMinutes)
		{}
		
		const std::string& getName() const { return m_name; }
		const int getDurationInMinutes() const { return m_durationInMinutes; }
	private:
		std::string m_name;
		int m_durationInMinutes;
	};

	
}

namespace std
{
	template <>
	struct hash<templates_course::Song>
	{
		size_t operator()(const templates_course::Song& song) const
		{
			size_t name = 31 * std::hash<int>{}(song.getDurationInMinutes());
			return name;
		}
	};
}

int main()
{
	std::unordered_set<templates_course::Song> myPersonalPlaylist;
	//Will this compile?
	return 0;
}