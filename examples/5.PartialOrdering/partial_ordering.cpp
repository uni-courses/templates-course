#include <iostream>

namespace templates_course
{
	template <typename T>
	void function(T)
	{
		std::cout << "The least specialized function was called." << std::endl;d
	}

	template <typename T>
	void function(T*)
	{
		std::cout << "The pointer function was called." << std::endl;
	}

	template <typename T>
	void function(const T*)
	{
		std::cout << "The  const-data-pointer function was called." << std::endl;
	}
}

int main() 
{
	int x = 0;
	const int y = 0;
	int* z = &x;
	const int* w = &y;
	int* const a = &x;

	templates_course::function(x);
	templates_course::function(z);
	//Ar trebui a 2 a
	templates_course::function(w);
	//Ar trebui a 2a
	templates_course::function(a);
	//Ar trebui prima

	return 0;
}