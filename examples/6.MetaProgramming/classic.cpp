#include <iostream>

namespace templates_course
{
	template <int N>
	struct factorial
	{
		static const unsigned long value = N * factorial<N - 1>::value;
	};

	template <>
	struct factorial<1>
	{
		static const unsigned long value = 1;
	};

	template <int N>
	struct fibonacci
	{
		static const unsigned long value = fibonacci<N - 1>::value + fibonacci<N - 2>::value;
	};

	template <>
	struct fibonacci<1>
	{
		static const unsigned long value = 1;
	};

	template <>
	struct fibonacci<0>
	{
		static const unsigned long value = 0;
	};
}


int main()
{
	//std::cout << templates_course::factorial<5>::value << std::endl;

		//Let's make these lines work!
	std::cout << templates_course::fibonacci<6>::value << std::endl;
	//std::cout << templates_course::fibonacci_v<6> << std::endl;
	return 0;
}