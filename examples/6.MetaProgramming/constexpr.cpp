#include <iostream>
#include <array>

namespace templates_course
{
	constexpr int factorial(int number)
	{
		if (number <= 0)
			return 1;
		return number * factorial(number - 1);
	}
}
//
//int main()
//{
//	constexpr int factorialValue = templates_course::factorial(11);
//
//	//std::cout << templates_course::fibonacci(5) << std::endl;
//	//Let's also make this line work!
//	return 0;
//}

