#include <algorithm>
#include <array>

#include <gcem.hpp>

#include <cmath>

namespace templates_course
{
	constexpr int kNotesCount = 88;

	constexpr double computeFrequency(int noteNumber)
	{
		return gcem::pow(2, (noteNumber - 49) / 12.0) * 440.0;
	}

	constexpr std::array<double, kNotesCount> computeFrequencies()
	{
		std::array<double, kNotesCount> values{ 0 };
		for (size_t index = 1; index <= kNotesCount; ++index)
			values[index - 1] = computeFrequency(index);
		
		return values;
	}
}

int main()
{
	// https://en.wikipedia.org/wiki/Piano_key_frequencies

	// 40

	constexpr auto values = templates_course::computeFrequencies();
	return 0;
}