#include <iostream>
#include <vector>

namespace templates_course
{
	
	int addNumbers(int x, int y)
	{
		return x + y;
	}

	int addNumbers(int x, int y, int z)
	{
		return x + y + z;
	}

	int addNumbers(int x, int y, int z, int k)
	{
		return x + y + z + k;
	}
}

//int main() 
//{
//	/*std::vector<int> x;
//	x.emplace_back();*/
//
//	std::cout << templates_course::addNumbers(1, 2) << '\n'
//			  << templates_course::addNumbers(1, 2, 3) << '\n'
//			  << templates_course::addNumbers(1, 2, 3, 4) << '\n';
//
//	return 0;
//}