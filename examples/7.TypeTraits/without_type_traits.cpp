#include <type_traits>
#include <iomanip>
#include <iostream>

namespace templates_course
{
	template <typename T>
	bool areEqual(const T& first, const T& second)
	{
		return first == second;
	}

}

//int main()
//{
//	std::cout << std::boolalpha
//		<< templates_course::areEqual(1, 1) << std::endl
//		<< templates_course::areEqual(1, 2) << std::endl
//		<< templates_course::areEqual(1.5, 1.5) << std::endl
//		<< templates_course::areEqual(1.12345678, 1.123456789) << std::endl;
//	//What will be the output?
//	return 0;
//}