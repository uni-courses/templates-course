#include <type_traits>
#include <iomanip>
#include <iostream>


namespace templates_course
{
	template <typename T>
	bool areEqual(const T& first, const T& second)
	{
		if (std::is_floating_point_v<T>)
		{
			float difference = std::abs(first - second);
			static constexpr float kEpsilon = 0.000001f;
			return difference < kEpsilon;
		}
		/*if (std::is_same_v<T, float>)
		{
			
		}
		else if  (std::is_same_v<T, double>)
		{
			double difference = std::abs(first - second);
			static constexpr double kEpsilon = 0.0000001;
			return difference < kEpsilon;
		}*/
		return first == second;
	}

}


int main()
{
	std::cout << std::boolalpha
		<< templates_course::areEqual(1, 1) << std::endl
		<< templates_course::areEqual(1, 2) << std::endl
		<< templates_course::areEqual(1.5, 1.5) << std::endl
		<< templates_course::areEqual(1.12345678, 1.123456789) << std::endl;
	//What will be the output?


	return 0;
}