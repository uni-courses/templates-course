#include <iostream>
#include <string>
#include <concepts>

template<typename T>
concept Stringable = requires(T a) 
{  
	{a.toString()}->std::convertible_to<std::string>;
};

class A 
{
private:
	std::string toString() const
	{
		return "This is class A!";
	}
};

class B 	
{
public:
	std::string toString() const
	{
		return "This is class B!";
	}
};

template <typename T>
void printWithPlainTemplate(T param)
{
	std::cout << param.toString() << std::endl;
}

template <typename T> requires Stringable<T>
void printWithConstraintedTemplate(T parameter)
{
	std::cout << parameter.toString();
}

int main()
{
	A a;
	B b;

	//printWithPlainTemplate(a);
	printWithConstraintedTemplate(a);
	return 0;
}