﻿# Building the examples
Assuming that you have `git` and `CMake` installed and in your PATH system variable, do the following:

 1. Open a cmd/terminal where you would like to clone the repo.
 2. Clone it: `git clone https://gitlab.com/ISilviu/templates-course.git`
 3. Navigate to the examples directory: `cd templates-course/examples`
 4. Create a new directory called build (this will contain the .sln and all the .vcxproj files): `md build`
 5. Navigate to it: `cd build`
 6. Use CMake to generate the project files: `cmake -G "Visual Studio [XX] [YYYY]" ../`
     - where XX stands for the Visual Studio version, for example: 16, 15 etc.
     - YYYY stands for the release year of that version, for example: 2019, 2017
     - for those using VS 2019, use the following command: `cmake -G "Visual Studio 16 2019" ../`
     - for those using VS 2017, use the following command: `cmake -G "Visual Studio 15 2017 Win64" ../`
 7. If everything went flawlessly, you should see your .sln file in the build folder. Open it and have fun with the examples!

 # Solutions
 The solutions we discussed at the course can be found on the "solutions" branch.

 # Troubleshooting
 If you have problems building the examples, don't hesitate to contact me at silviu.ifrim@student.unitbv.ro.

